This is a test I make to improve my skills with React and the whole ecosystem behind it

It involves for the moment :
- Yarn
- React
- Webpack
- Babel

This project works with docker, you can start it easily with : 
```
make install
```

Then go to http://localhost/app.html

See Makefile for other commands


Enjoy :)