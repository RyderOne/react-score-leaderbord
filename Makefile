step=----------------
compose=docker-compose
project=react-score-leaderboard


SUPPORTED_COMMANDS := yarn-add npm-install
SUPPORTS_MAKE_ARGS := $(findstring $(firstword $(MAKECMDGOALS)), $(SUPPORTED_COMMANDS))
ifneq "$(SUPPORTS_MAKE_ARGS)" ""
  # use the rest as arguments for the command
  COMMAND_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  COMMAND_ARGS := $(subst :,\:,$(COMMAND_ARGS))
  COMMAND_ARGS := $(subst -,\-,$(COMMAND_ARGS))
  COMMAND_ARGS := $(subst =,\=,$(COMMAND_ARGS))
  # ...and turn them into do-nothing targets
  $(eval $(COMMAND_ARGS):;@:)
endif


install: build yarn-install start

build: remove
	@echo "$(step) Building images docker $(step)"
	@$(compose) build

build-no-cache: remove
	@echo "$(step) Building images docker $(step)"
	@$(compose) build --no-cache

up:
	@echo "$(step) Starting $(project) $(step)"
	@$(compose) up -d node

start: up

restart: stop start

stop:
	@echo "$(step) Stopping $(project) $(step)"
	@$(compose) stop

remove: stop
	@echo "$(step) Remove $(project) $(step)"
	@$(compose) rm --force

bash:
	@echo "$(step) Bash $(project) $(step)"
	@docker exec -it reactscoreleaderbord_node_1 bash

yarn-install:
	@$(compose) run node yarn

yarn-add:
	@$(compose) run node yarn add $(COMMAND_ARGS)

npm-install:
	@$(compose) run node npm install $(COMMAND_ARGS)