const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
  template: './src/app.html',
  filename: 'app.html',
  inject: 'body'
});

module.exports = {
    entry: "./src/app.js",
    output: {
        path: path.resolve(__dirname, 'bin'),
        filename: "app.bundle.js"
    },
    module: {
        rules: [
            { test: /\.js$/, exclude: /node_modules/, loader: "babel-loader" },
            { test: /\.jsx$/, exclude: /node_modules/, loader: "babel-loader" }
        ]
    },
    plugins: [HtmlWebpackPluginConfig],
    devServer: {
        host: '0.0.0.0',
        port: 80
    }
};